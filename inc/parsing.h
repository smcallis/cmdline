#pragma once

// c++
#include <regex>
#include <memory>
#include <string>
#include <functional>
#include <type_traits>

// XXX:
#include <assert.h>

// contains an implementation of parser combinators for PEGs.

// define macros for defining template definition of a combinator (combines two parsers into one)
// and transformers (converts one parser into another)
//
// it would be better to do this with a default parameter for B (void), but if you do that
// template argument deduction for B is disabled *sigh*
//
// a COMBINATOR takes two parsers (A,B) and combines them into a new parser
// a TRANSMUTOR takes a single parser and modifies it in some way, returning a new parser
//

namespace parsing {
    using std::regex;
    using std::string;
    using std::vector;
    using std::function;
    using std::is_same;
    using std::enable_if;
    using std::is_convertible;
    using std::shared_ptr;    
    using std::make_shared;
    
    // index/row/column of a parse
    struct parse_pos {
        ssize_t idx = 0;
        ssize_t row = 0;
        ssize_t col = 0;
    };


    // stream abstraction so we don't have to constantly copy strings, allows us to
    // advance a pointer on a shared copy of the string while not modifying it.
    struct parse_stream {
        parse_stream() =default;

        parse_stream(const char* str) 
            : parse_stream(string(str)) {}

        parse_stream(string str)
            : str_(std::make_shared<string>(str)) {}

        // de-reference stream to get current character
        char operator *() const {
            return (*str_)[pos_.idx];
        }

        // return iterators into stream, mostly for regex_match
        string::const_iterator begin() const { return str_->begin() + pos_.idx; }
        string::const_iterator end()   const { return str_->end();              }

        // return current position in stream
        parse_pos position() const {
            return pos_;
        }

        // peek at data at start of stream, negative number indicates rest of stream
        string peek(ssize_t num=1) const {
            return str_->substr(pos_.idx, (num > 0) ? num : string::npos);
        }

        // return rest of stream 
        string rest() const {
            return peek(0);
        }

        // create a new stream by advancing current stream
        parse_stream advance(ssize_t num=1) const {
            // don't move backwards
            if (num < 0) {
                return *this;
            }

            // copy current stream state
            return parse_stream(*this).munch(num);
        }

    private:
        // advance internal pointers by a given number of characters, handle
        // counting rows and columns as we go.
        parse_stream& munch(ssize_t num) {
            for (ssize_t ii=0; ii < num; ii++) {
                if (pos_.idx < (ssize_t)str_->size()) {
                    pos_.col++;
                    if ((*str_)[pos_.idx++] == '\n') {
                        pos_.row++;
                        pos_.col = 0;
                    }
                }
            }

            return *this;
        }

        parse_pos                pos_; // current position in stream
        vector<parse_pos>      stack_; // stack of positions
        shared_ptr<const string> str_; // underlying buffer
     };


    // sum type to hold the result of a parse
    struct parse_result {
        parse_result()        : parse_result(false) {}
        parse_result(bool ok) : ok_(ok)             {}

        // return a valid result
        static inline parse_result ok(string value) {
            return parse_result(true).payload(value);
        }

        // return an error result
        static inline parse_result error(string error) {
            return parse_result(false).payload(error);
        }

        // explicit bool operator to check status
        operator bool() const { return ok_; }

        // accessors
        bool   ok()    const { return ok_;      }
        string value() const { return payload_; }
        string error() const { return payload_; }

        // for an OK result, return how many bytes we consumed
        ssize_t consumed() const {
            return ok_ ? payload_.size() : 0;
        }

        // set the payload
        parse_result& payload(string payload) {
            payload_ = payload;
            return *this;
        }

    private:
        bool      ok_;
        string    payload_;
        parse_pos pos_;
    };
    

    // disallow wrapping generic type in parser
    template <typename P, typename allow=void>
    struct parser;

    
    // build parser from callable
    template <typename P>
    struct parser<P, typename enable_if<is_convertible<P, function<parse_result(parse_stream)>>::value>::type> {
        parser(P p)
            : p_(p) {}
        
        // just call contained parser
        parse_result operator()(parse_stream stream) {
            return p_(stream);
        }
        
    private:
        P p_;
    };


    // build parser from string
    template <>
    struct parser<string> {
        parser(string text)
            : text_(text) {}

        // match string against stream
        parse_result operator()(parse_stream stream) {
            string next = stream.peek(text_.size());
            if (next == text_) {
                return parse_result::ok(next);
            }
            return parse_result::error("expected '" + text_ + "', saw '" + next + "'");
        }
    private:
        string text_;
    };


    // build parser from c-string
    template <>
    struct parser<const char*> : parser<string> {
        parser(const char* str)
            : parser<string>(str) {}
    };
    
    
    // a parser is anything we can call with a parse_stream and get a parse_result back.
    // this lets us disable substitution for things like the | operator, where allowing any
    // type would conflict with a lot of things.
    //
    template <typename A, typename B=void>
    using is_parser =
        std::enable_if<
           std::is_convertible<A, parser<A>>::value && 
          (std::is_convertible<B, parser<B>>::value || std::is_void<B>::value)
        >;

#define COMBINATOR template <typename A, typename B, typename is_parser<A,B>::type* = nullptr>
#define TRANSMUTOR template <typename A, typename is_parser<A>::type* = nullptr>

    /*******************************************************************************
     * define combinators
     *******************************************************************************/

    // accept either of two parsings, try A first then B
    COMBINATOR struct either_ {
        either_(A a, B b)
            : pa_(a), pb_(b) {}

        // merge two parser names
        string name() const {
            //return "(" + pa_.name() + "|" + pb_.name() + ")";
            return "undefined";
        }

        // execute
        parse_result operator()(parse_stream stream) {
            parse_result resa = pa_(stream); if (resa) return resa;
            parse_result resb = pb_(stream); if (resb) return resb;
            return parse_result::error("expected " + name());
        }

    private:
        parser<A> pa_;
        parser<B> pb_;
    };

    COMBINATOR either_<A,B> either(A a, B b) { return either_<A,B>(a,b); }
    
    

    // require both of two parsings, first a then b
    COMBINATOR struct both_ {
        both_(A a, B b)
            : pa_(a), pb_(b) {}

        // merge two parser names
        string name() const {
            return pa_.name() + " & " + pb_.name();
        }

        // execute
        parse_result operator()(parse_stream stream) {
            // try to parse a first, return error if failure
            parse_result resa = pa_(stream);
            if (!resa) {
                return resa;
            }

            // if a succeeded, try to parse b too on advanced stream
            parse_result resb = pb_(stream.advance(resa.consumed()));
            if (!resb) {
                return resb;
            }

            // success, return parse
            return parse_result::ok(resa.value() + resb.value());
        }
    private:
        parser<A> pa_;
        parser<B> pb_;
    };

    COMBINATOR both_<A,B> both(A a, B b) { return both_<A,B>(a,b); }

    
    // negate the result of a parser
    TRANSMUTOR struct notparser_ {
        notparser_(A a)
            : pa_(a) {}

        // generate parser name
        string name() const {
            return "!" + pa_.name();
        }

        // execute
        parse_result operator()(parse_stream stream) {
            if (pa_(stream)) {
                return parse_result::error("parser matched");
            }
            return parse_result::ok("no match");
        }

    private:
        parser<A> pa_;
    };

    TRANSMUTOR notparser_<A> notparser(A a) { return notparser_<A>(a); }
    

    // many applies a parser repeatedly to the input, max of zero implies no limit
    TRANSMUTOR struct many_ {
        many_(A a, ssize_t min=1, ssize_t max=0)
            : pa_(a), min_(min), max_(max) {}

        // generate regex-like name for rule
        string name() const {
            return pa_.name() + "{" + std::to_string(min_) + "," + std::to_string(max_) + "}";
        }

        // execute
        parse_result operator()(parse_stream stream) {
            parse_result res;
            ssize_t cnt = 0;
            string  result;
            while (max_ == 0 || cnt < max_) {
                stream = stream.advance(res.consumed());
                res    = pa_(stream);
                if (!res) {
                    break;
                }
                result += res.value();
                cnt++;
            }

            // done here, if we got enough results, then return result
            if (cnt >= min_) {
                return parse_result::ok(result);
            }
            //return parse_result::error("expected (" + pa_.name() + "){" + std::to_string(min_) + ",}");
            return parse_result::error("not implemented");
        }

    private:
        parser<A> pa_;
        ssize_t min_, max_;
    };

    TRANSMUTOR many_<A> many(A a, ssize_t min=1, ssize_t max=0) { return many_<A>(a, min, max); }
    
    
    // define combinator operators
    COMBINATOR    either_<A,B> operator |(A a, B b) { return either(a,b);  } // parse a or b
    COMBINATOR      both_<A,B> operator ,(A a, B b) { return both(a,b);    } // parse a and b
    COMBINATOR      both_<A,B> operator &(A a, B b) { return both(a,b);    } // parse a and b
    TRANSMUTOR notparser_<A>   operator !(A a)      { return notparser_<A>(a); } // succeed when a fails, consume no input
    TRANSMUTOR      many_<A>   operator *(A a)      { return many(a, 0);   } // kleene-star, match zero or more
    TRANSMUTOR      many_<A>   operator +(A a)      { return many(a, 1);   } // + operator, match one or more


    /*******************************************************************************
     * define basic parsers
     *******************************************************************************/

    // rename a parser
    TRANSMUTOR struct rename_ {
        rename_(A a) 
            : a_(a) {}
        
        // execute parser
        parse_result operator()(parse_stream stream) {
            return a_(stream);
        }
        
    private:
        parser<A> a_;
    };

    TRANSMUTOR rename_<A> rename(A a) { return rename_<A>(a); }
    
    
    // anything parser always matches any string
    struct anything_p { 
        parse_result operator()(parse_stream stream) {
            return parse_result::ok(stream.rest());
        }
    } anything;
    
    // nothing parser never matches any string
    auto nothing = !anything;

    
    // term parser matches a string literal exactly
    struct term {
        term() =default;
        term(string text)
            : text_(text) {}

        // execute parser
        parse_result operator()(parse_stream stream) {
            string next = stream.peek(text_.size());
            if (next == text_) {
                return parse_result::ok(next);
            }
            return parse_result::error("expected '" + text_ + "', saw '" + next + "'");
        }

    private:
        string text_;
    };
       

    // matches a character based on a predicate function
    struct predicate {
        predicate(int (*pred)(int c), string name="predicate")
            : pred_(pred), name_(name) {}

        // yield predicate name
        string name() const {
            return name_;
        }

        // execute parser
        parse_result operator()(parse_stream stream) {
            if (pred_ && pred_(*stream))
                return parse_result::ok(stream.peek(1));
            return parse_result::error("expected " + name());
        }

    private:
        int (*pred_)(int c);
        string name_;
    };

    
    // instantiate common predicate parsers
    auto alnum = predicate(isalnum, "alphanumeric");
    auto alpha = predicate(isalpha, "letter");
    auto digit = predicate(isdigit, "digit");
    auto lower = predicate(islower, "lower case");
    auto upper = predicate(isupper, "upper case");
    auto space = predicate(isspace, "space");
    auto blank = predicate(isblank, "blank");


    // regexp matches against a regular expression
    struct regexp {
        regexp(string exp)
            : exp_(exp), regex_(exp) {}

        // yield regexp name
        string name() const {
            return exp_;
        }

        // execute parser
        parse_result operator()(parse_stream stream) {
            using std::regex;
            using std::smatch;
            using std::regex_search;

            smatch matches;
            if (regex_search(stream.begin(), stream.end(), matches, regex_)) {
                return parse_result::ok(
                    matches.str(0)
                );
            }
            return parse_result::error("no match for '" + exp_ + "'");
        }

    private:
        string exp_;
        regex  regex_;
    };


    // capture binds a callable to a parser so that if the parser succeeds,
    // the callable receives the parsed string and its location in the stream.
    // struct capture {
    //     typedef function<void(parse_pos, string)> capture_cb;

    //     // capture a parser with given callback
    //     capture(parser p, capture_cb callback)
    //         : p_(p), callback_(callback) {}

    //     // just yield captured parser's name
    //     string name() const override {
    //         return p_.name();
    //     }

    //     // execute parser, if it succeeds, pass result to capture
    //     parse_result operator()(parse_stream stream) override {
    //         parse_result res = p_(stream);
    //         if (res && callback_) {
    //             callback_(stream.position(), res.value());
    //         }
    //         return res;
    //     }

    // private:
    //     parser     p_;
    //     capture_cb callback_;
    // };    

    // token makes a parser not sensitive to leading whitespace
    TRANSMUTOR auto token(A p) -> decltype(*space & p) {
        return *space & p;
    }
}


#ifdef DOCTEST_LIBRARY_INCLUDED
namespace parsing {
    TEST_SUITE("parser combinator") {
        TEST_CASE("basic parsers") {
            SUBCASE("predicate parser") {
                REQUIRE(!!alpha("abcd"));
                REQUIRE( !alpha("1234"));
                REQUIRE(!!alnum("1234"));
                REQUIRE(!!digit("1234"));
            }


            SUBCASE("term parser") {
                REQUIRE(!!term("test")("testing 1 2 3"));
                REQUIRE( !term("test")("  testing 1 2 3"));
                REQUIRE(!!term("foob")("foobar baz buz bizzle"));
                REQUIRE( !term("foob")("\tfoobar baz buz bizzle"));
                REQUIRE( !term("test")("ich bin ein Berliner"));
                REQUIRE( !term("2.71")("3.141592654"));
            }


            SUBCASE("regexp parser") {
                REQUIRE(!!regexp("^test$")("test"));
                REQUIRE( !regexp("^test$")("testing"));
                REQUIRE(!!regexp("^test")("testing"));
                REQUIRE( !regexp("[0-9]+")(""));
                REQUIRE(!!regexp("[0-9]+")("1"));

                // string result;
                // auto match = capture(regexp("[0-9]+"),
                //     [&result](parse_pos, string value) {
                //         result = value;
                //     }
                // );

                // REQUIRE(  !!match("123456789"));
                // REQUIRE(result == "123456789");
            }
        }


        TEST_CASE("combinators") {
            SUBCASE("| combinator") {
                REQUIRE(!!(term("hello") | term("world"))("hello world"));
                REQUIRE(!!(term("hello") | term("world"))("world hello"));
                REQUIRE( !(term("hello") | term("world"))("foo bar baz"));
            }


            SUBCASE("& combinator") {
                REQUIRE(!!(term("hello") & term("world"))("helloworld"));
                REQUIRE( !(term("hello") & term("world"))("hellothere"));
                REQUIRE( !(term("hello") & term("world"))("g'daythere"));
            }


            SUBCASE(", combinator") {
                REQUIRE(!!(term("hello") , term("world"))("helloworld"));
                REQUIRE( !(term("hello") , term("world"))("hellothere"));
                REQUIRE( !(term("hello") , term("world"))("g'daythere"));
            }


            SUBCASE("! combinator") {
                REQUIRE( !(!(term("hello") | term("world")))("hello world"));
                REQUIRE( !(!(term("hello") | term("world")))("world hello"));
                REQUIRE(!!(!(term("hello") | term("world")))("foo bar baz"));
                REQUIRE( !(!(term("hello") & term("world")))("helloworld" ));
                REQUIRE(!!(!(term("hello") & term("world")))("hellothere" ));
                REQUIRE(!!(!(term("hello") & term("world")))("g'daythere" ));
                REQUIRE( !(!(term("hello") , term("world")))("helloworld" ));
                REQUIRE(!!(!(term("hello") , term("world")))("hellothere" ));
                REQUIRE(!!(!(term("hello") , term("world")))("g'daythere" ));
            }


            SUBCASE("* combinator (kleene-star)") {
                REQUIRE(!!(*token(regexp("[0-9]+")))(" testing "));
                REQUIRE(!!(*token(regexp("[0-9]+")))("12345"));
                REQUIRE(!!(*token(regexp("[0-9]+")))("12345 3 23 91348 0"));
                REQUIRE(!!(*token(regexp("[0-9]+")))("\t12345"));
                REQUIRE(!!(*token(regexp("[0-9]+")))(""));

                parse_result res = (*token("abcd"))("  abcd  abcd  abcd");
                REQUIRE(!!res);
                REQUIRE(res.value() == "  abcd  abcd  abcd");
            }


            SUBCASE("+ combinator (one-or-more)") {
                REQUIRE( !(+token(regexp("[0-9]+")))(" testing "));
                REQUIRE(!!(+token(regexp("[0-9]+")))("12345"));
                REQUIRE(!!(+token(regexp("[0-9]+")))("12345 3 23 91348 0"));
                REQUIRE(!!(+token(regexp("[0-9]+")))("\t12345"));
                REQUIRE( !(+token(regexp("[0-9]+")))(""));

                parse_result res = (+token("abcd"))("  abcd  abcd  abcd");
                REQUIRE(!!res);
                REQUIRE(res.value() == "  abcd  abcd  abcd");
            }


            SUBCASE("token combinator") {
                REQUIRE(!!(token("test")("testing 1 2 3")));
                REQUIRE(!!(token("test")("  testing 1 2 3")));
                REQUIRE(!!(token("foob")("foobar baz buz bizzle")));
                REQUIRE(!!(token("foob")("\tfoobar baz buz bizzle")));
                REQUIRE( !(token("test")("ich bin ein Berliner")));
                REQUIRE( !(token("2.71")("3.141592654")));
            }


            SUBCASE("many combinator") {
                REQUIRE( !many(token(regexp("[0-9]+")))(" testing "));
                REQUIRE(!!many(token(regexp("[0-9]+")))("12345"));
                REQUIRE(!!many(token(regexp("[0-9]+")))("12345 3 23 91348 0"));
                REQUIRE(!!many(token(regexp("[0-9]+")))("\t12345"));
                REQUIRE(!!many(token(regexp("[0-9]+")))(" 12345"));
            }
        }
    }
}
#endif
