#define DOCTEST_CONFIG_WITH_NULLPTR
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

//#include <cmdline.h>
#include <parsing.h>

// #include <sys/types.h>
// #include <sys/wait.h>
// #include <unistd.h>
// #include <libgen.h>

// int child(const char* args, bool suppress=true) {
//     if (fork() == 0) {
//         if (suppress) {
//             stdout = freopen("/dev/null", "w", stdout);
//             stderr = freopen("/dev/null", "w", stderr);
//         }
//         execlp("bin/testharn", "testharn", args, NULL);
//     }

//     int stat; wait(&stat);
//     return stat;
// }

// int main() {
//     //char const* argv[] = {"testing", "one", "two", "three"};

//     struct {
//         const char* spec;
//         bool        succeed;
//     } tests[] = {
//         {"_Arguments\n" // cmdline.h:539 - single vector argument
//          "  <input0+> - first input\n"
//          "  <input1+> - second input\n",                              false},

//         {"_Arguments\n" // cmdline.h:546 - expected '>'
//          "  <input0 - first input\n"
//          "  <input1> - second input\n",                               false},

//         {"_Arguments\n" // cmdline.h:548 - description required
//          "  <input0+> - first input\n"
//          "  <input1> \n",                                             false},

        
//         {"_Options\n" // cmdline.h:571 - expect short option name
//          "  --progress/-p - print progress to stderr\n"
//          "  --limit/=   - limit total number of lines in output\n",   false}, 

//         {"_Options\n" // cmdline.h:576 - expect short option name
//          "  --progress/-p - print progress to stderr\n"
//          "  -%            - limit total number of lines in output\n", false}, 

//         {"_Options\n" // cmdline.h:585 - check for duplicate option definitions
//          "  --progress/-p - print progress to stderr\n"
//          "  --progress    - progress again\n",                        false}, 

//         {"_Options\n" // same 
//          "  --progress/-p - print progress to stderr\n"
//          "  --limit/-l=   - limit total number of lines in output\n"
//          "  --label/-l=   - label to test string options\n",          false},

//         {"_Options\n" // cmdline.h:591 - option description required
//          "  --progress/-p - print progress to stderr\n"
//          "  --limit/-l=\n",                                           false},

//         {"_Options\n" // cmdline.h:598 - reserved option name
//          "  --help/-h - print help\n",                                false},

//         {"_Parameters\n"
//          "  <output>  - Filename to receive CAF output (type 2000 CF)\n"
//          "  <inputs+> - First input file to correlate  (type 1000 CF)\n"
//          "  <input1>  - Second input file to correlate (type 1000 CF)\n"
//          "\n"
//          "_Options\n"
//          "  --time        - print execution time before exiting\n"
//          "  --progress/-p - display CAF calculation progress on stdout         \n"
//          "  --beatfreq    - use beat frequency algorithm                       \n"
//          "  --threads/-t= - number of threads to use, (default: 1)             \n"
//          "  --nommap      - disable memory mapping, read data into intermediate buffer first\n"
//          "\n"
//          "  --window=   - use a window+1 tap raised cosine window to reduce ringing\n"
//          "                in time and frequency.                               (default: 0)\n"
//          "  --chirp=    - amount of chirp to apply before CAF                  (default: 0.0 Hz/s)\n"
//          "  --tspan=    - length of time to use for CAF                        (default: all)  \n"
//          "\n"
//          "  --tmin=     - minimum time offset to search                        (default: -0.1s)\n"
//          "  --tmax=     - maximum time offset to search                        (default: +0.1s)\n"
//          "  --tover=    - time oversample to use                               (default: 2)    \n"
//          "\n"
//          "  --fmin=     - minimum frequency offset to search                   (default: -50Hz)\n"
//          "  --fmax=     - maximum frequency offset to search                   (default: +50Hz)\n"
//          "  --fover=    - frequency oversampling to use                        (default: 2)    \n"
//              "\n"
//          "  --declen=   - decimation filter length in samples                  (default: 'decimation' samples)\n"
//          "  --decwin=   - window to use when generating decimation filter\n"
//          "  --decbw=    - normalized bandwidth (0,1] for the decimation filter (default: 1.0)\n", true},
//     };

//     for (size_t ii=0; ii < sizeof(tests)/sizeof(tests[0]); ii++) {
//         int status = child(tests[ii].spec);
//         printf("status: %i\n", status);
        
//         if (( tests[ii].succeed && (status != 0)) ||
//             (!tests[ii].succeed && (status == 0))) {
//             fprintf(stderr, "test %zd failed -\n", ii);
//             fprintf(stderr, "%s", tests[ii].spec);
//             fprintf(stderr, "Program output:\n\n");
//             child(tests[ii].spec, false);            
//             exit(EXIT_FAILURE);
//         } else {
//             fprintf(stderr, "test %zd passed\n", ii);
//         }
//     }
// }
