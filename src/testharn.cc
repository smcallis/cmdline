#include <cmdline.h>

int main(int argc, const char* argv[]) {
    // swap spec to argv[0], and program name up one to make args contiguous
    std::swap(*argv, *(argv+1));
    cmdline args(argc-1, argv+1, argv[0]);
    return 0;
}
