#include <cmdline.h>
#include <terminal.h>

using namespace command_line;


bool spec_test(const char* line, bool should_pass=false) {
    static int cnt = 0;
    
    bool passed = should_pass;
    try {
        cmdline_spec spec(line, true);
    } catch (parse_error err) {
        passed = !should_pass;
    }

    if (!passed) {
        printf("[%3d] failed - '%s'\n", cnt, line);
    } else {
        printf("[%3d] passed\n", cnt);
    }

    cnt++;
    return passed;
}


bool cmdline_test(const char* spec, int argc, const char* argv[], bool should_pass=false) {
    static int cnt = 0;
    
    bool passed = should_pass;
    try {
        cmdline args(argc, argv, spec, CMD_THROWERROR);
    } catch (std::runtime_error err) {
        passed = !should_pass;
    }

    if (!passed) {
        printf("[%3d] failed\n", cnt);
    } else {
        printf("[%3d] passed\n", cnt);
    }

    cnt++;
    return passed;
}


int main() {
    command_line::parse_spec("");
    return 0;
        
    bool passed = true;

    // test cmdline_spec
    struct {
        const char* test;
        bool should_pass;
    } spec_tests[] = {
        // test simple text line and header line
        { "this is a text line, it should pass",                    true  },
        { "_Test Header With Spaces basically can't fail",          true  },

        // test argument parsing
        { "<output>   - output filename",                           true  },
        { "<out+put>  - bad character in name",                     false },
        { "<output1+> - filename\n"
          "<output2+> - multiple vector arguments not allowed\n",   false },
        { "<output    - non-terminated option",                     false },
        { "<output>   -  ",                                         false },
        { "<output>",                                               false },

        // test option parsing
        { "--option/-o - description",                              true  },
        { "-o/--option - description",                              false },
        { "-o - description",                                       true  },
        { "--option/-o= - description",                             true  },
        { "-o= - description",                                      true  },
        { "--option/-o - description\n"
          "--option - blah",                                        false }, // no duplicate options
        { "--option/-o - description\n"
          "-o - blah",                                              false }, // no duplicate options
        { "--option/",                                              false },
        { "--option - description",                                 true  },
        { "--op%ion",                                               false }, // bad character in name
        { "--option/-",                                             false },
        { "--option/-#",                                            false },
        { "-#",                                                     false },
        { "--help - help",                                          false },
        { "-h - help",                                              false },
        { "--help/-h  - help",                                      false },
        { "--help/-h= - help",                                      false },
    };

    for (size_t ii=0; ii < sizeof(spec_tests)/sizeof(spec_tests[0]); ii++) {
        passed &= spec_test(spec_tests[ii].test, spec_tests[ii].should_pass);
    }


    // test cmdline proper
    const char* spec = \
        "This is an example program showing how to use cmdline.h.  It's just a theoretical program\n"
        "that multiplexes multiple files into one file.\n"
        "The program spec is specified as a string containing information about arguments and options\n\n"

        "_Parameters\n"
        "  <output>  - output filename to write\n"
        "  <inputs+> - input filenames to merge\n"
        "  <onemore> - extra filename after inputs\n"

        "_Options\n"
        "   -f           - option 1\n"
        "   -e           - option 2\n"
        "   -g           - option 3\n"
        "  --test        - no short option\n"
        "  --progress/-p - print progress to stderr\n"
        "  --limit/-l=   - limit total number of lines in output\n"
        "  --label/-a=   - label to test string options\n";

    struct {
        int   argc;
        const char* argv[16];
        bool should_pass;
    } cmd_tests[] = {
        { 1,  { "program" },                                                               false }, 
        { 2,  { "program", "out"  },                                                       false },  // not enough inputs
        { 3,  { "program", "out", "in0"  },                                                false },  // not enough inputs
        { 4,  { "program", "out", "in0", "one" },                                          true  },  // single inupt for multiarg
        { 5,  { "program", "out", "in0", "in1", "one" },                                   true  },  // multi-args for multiarg
        { 6,  { "program", "out", "in0", "one", "-p", "--progress" },                      true  },  // multiple instances OK
        { 8,  { "program", "out", "in0", "-l", "3", "one", "-p", "--progress" },           true  },  // specify options wherever
        { 8,  { "program", "out", "in0", "-l", "3", "one", "-p", "--progress", "-l=234" }, true  },  // multiple value is OK
        { 7,  { "program", "out", "in0", "one", "-p", "--progress", "-l"},                 false },  // value option requires value
        { 5,  { "program", "out", "in0", "one", "-pl" },                                   false },  // can't put value options in compound args
        { 5,  { "program", "out", "in0", "one", "-fegp" },                                 true  },  // compound options OK
        { 5,  { "program", "out", "in0", "one", "-fegp", "-l=123" },                       true  },  // short option value
        { 10, { "program", "out", "in0", "-l", "--", "234", "one", "-fegp", "-l", "3"},    false },  // option won't consume --
    };

    for (size_t ii=0; ii < sizeof(cmd_tests)/sizeof(cmd_tests[0]); ii++) {
        passed &= cmdline_test(spec, cmd_tests[ii].argc, cmd_tests[ii].argv, cmd_tests[ii].should_pass);
    }
    
    // test retrieving results from cmdline
    {
        const char* argv[] = {"program", "out", "in0", "one", "-fegp", "-l", "234"};
        cmdline args(7, argv, spec);
        passed &=  string(argv[0]) == "program";
        passed &=  string(argv[1]) == "out";
        passed &=  string(argv[2]) == "in0";
        passed &=  string(argv[3]) == "one";
        passed &=  args.num_args() == 3;
        passed &=  args.optset("-f");
        passed &=  args.optset("-e");
        passed &=  args.optset("-g");
        passed &=  args.optset("-p");
        passed &=  args.optset("-l");
        passed &= !args.optset("--label");
        passed &=  args.optint("-l", 0) == 234;
        printf("%s -- full test\n", passed ? "passed" : "failed");
    }

    {
        const char* argv[] = {"program", "out", "in0", "one", "-fegp", "-l=234"};
        cmdline args(6, argv, spec);
        passed &=  args.num_args() == 3;
        passed &=  string(argv[0]) == "program";
        passed &=  string(argv[1]) == "out";
        passed &=  string(argv[2]) == "in0";
        passed &=  string(argv[3]) == "one";
        passed &=  args.optset("-f"); 
        passed &=  args.optset("-e"); 
        passed &=  args.optset("-g"); 
        passed &=  args.optset("-p"); 
        passed &=  args.optset("-l");
        passed &= !args.optset("--label");
        passed &=  args.optint("-l", 0) == 234;
        printf("%s -- full test\n", passed ? "passed" : "failed");
    }

    // test unknown option doesn't get set
    {
        const char* argv[] = {"program", "out", "in0", "-l", "234", "one", "-fegpq", "-q"};
        cmdline args(8, argv, spec);
        printf("%s -- full test\n", passed ? "passed" : "failed");
    }

    
    // test that last option specifies value
    {
        const char* argv[] = {"program", "out", "in0", "-l", "234", "one", "-fegp", "-l", "3"};
        cmdline args(9, argv, spec);
        passed &=  args.num_args() == 3;
        passed &=  string(argv[0]) == "program";
        passed &=  string(argv[1]) == "out";
        passed &=  string(argv[2]) == "in0";
        passed &=  args.optset("-f");
        passed &=  args.optset("-e");
        passed &=  args.optset("-g");
        passed &=  args.optset("-p");
        passed &=  args.optset("-l");
        passed &= !args.optset("--label");
        passed &=  args.optint("-l", 0) == 3;
        printf("%s -- full test\n", passed ? "passed" : "failed");
    }
    
    // test that -- stops parsing
    {
        const char* argv[] = {"program", "out", "in0", "-l", "234", "--", "one", "-fegp", "-l", "3"};
        cmdline args(10, argv, spec);
        passed &=  args.num_args() == 6;
        passed &=  string(argv[0]) == "program";
        passed &=  string(argv[1]) == "out";
        passed &=  string(argv[2]) == "in0";
        passed &= !args.optset("-f");
        passed &= !args.optset("-e");
        passed &= !args.optset("-g");
        passed &= !args.optset("-p");
        passed &= !args.optset("-a");
        passed &=  args.optint("-l", 0) == 234;
        printf("%s -- full test\n", passed ? "passed" : "failed");
    }
   
    // print results
    if (!passed) {
        printf("one or more tests failed\n");
    } else {
        printf("all tests passed\n");
    }
    
    return !passed;
}
